package com.example.intro.se.service;

public interface AccountService {
    String isLogin(String userName, String password);
}
