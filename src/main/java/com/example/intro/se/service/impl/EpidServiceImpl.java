package com.example.intro.se.service.impl;

import com.example.intro.se.domain.EpidReport;
import com.example.intro.se.dto.request.EpidCreateRequestDTO;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import com.example.intro.se.repository.EpidReportRepository;
import com.example.intro.se.repository.UserRepository;
import com.example.intro.se.service.EpidService;
import com.example.intro.se.utils.EpidUtils;
import com.example.intro.se.utils.UserUtils;
import com.example.intro.se.utils.sendmail.EmailThreadController;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
@AllArgsConstructor
public class EpidServiceImpl implements EpidService {
    private final EpidReportRepository epidReportRepository;
    private final UserRepository userRepository;

    @Override
    public Page<EpidReport> getAll(Pageable pageable) {
        return epidReportRepository.findAll(pageable);
    }

    @Override
    public EpidReport getById(Long id) {
        return epidReportRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Entity Not Found"));
    }

    @Override
    public List<EpidReport> getReportsByUserId(Long id) {
        return epidReportRepository.findAllByUserId(id);
    }

    @Override
    public EpidReport create(CreateReportRequestDTO<Long, EpidCreateRequestDTO> createRequestDTO) {
        if(!isExist(createRequestDTO.pk)){
            userRepository.save(UserUtils.toDAO(createRequestDTO.pk, createRequestDTO.user));
        }
        EmailThreadController.send(createRequestDTO.user.getTelNo());
        return epidReportRepository.save(EpidUtils.toDAO(createRequestDTO.pk, createRequestDTO.report));
    }
    boolean isExist(Long userId){
        return userRepository.findById(userId).isPresent();
    }

}
