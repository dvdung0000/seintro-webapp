package com.example.intro.se.service;

import com.example.intro.se.dto.request.UserCreateRequestDTO;
import com.example.intro.se.dto.request.UserUpdateRequestDTO;
import com.example.intro.se.dto.response.UserResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {
    Page<UserResponseDTO> getAll(Pageable pageable);
    UserResponseDTO getById(Long id);
    UserResponseDTO update(Long id, UserUpdateRequestDTO updateRequestDTO);
    UserResponseDTO create(Long id, UserCreateRequestDTO createRequestDTO);
    void delete(Long id);
}
