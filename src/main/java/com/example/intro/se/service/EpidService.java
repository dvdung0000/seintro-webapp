package com.example.intro.se.service;

import com.example.intro.se.domain.EpidReport;
import com.example.intro.se.dto.request.EpidCreateRequestDTO;
import com.example.intro.se.dto.request.Generic.CreateReportRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface EpidService {
    Page<EpidReport> getAll(Pageable pageable);
    EpidReport getById(Long id);
    List<EpidReport> getReportsByUserId(Long id);
    EpidReport create(CreateReportRequestDTO<Long, EpidCreateRequestDTO> createReportRequestDTO);
}
