package com.example.intro.se.service.impl;

import com.example.intro.se.domain.Init;
import com.example.intro.se.dto.request.InitCreateRequestDTO;
import com.example.intro.se.dto.request.InitUpdateRequestDTO;
import com.example.intro.se.dto.response.InitResponseDTO;
import com.example.intro.se.repository.InitRepository;
import com.example.intro.se.service.InitService;
import com.example.intro.se.utils.InitUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@AllArgsConstructor
public class InitServiceImpl implements InitService {

    private final InitRepository initRepository;

    @Override
    public Page<InitResponseDTO> findAll(Pageable pageable) {
        return initRepository.findAll(pageable).map(InitUtils::toDto);
    }

    @Override
    public InitResponseDTO create(InitCreateRequestDTO initCreateRequestDTO) {

        return InitUtils.toDto(initRepository.save(InitUtils.toEntity(initCreateRequestDTO)));
    }

    @Override
    public InitResponseDTO update(Long id, InitUpdateRequestDTO initUpdateRequestDTO) {
        return InitUtils.toDto(initRepository.save(InitUtils.toEntity(getEntityById(id), initUpdateRequestDTO)));
    }

    @Override
    public InitResponseDTO delete(Long id) {
        Init temp = getEntityById(id);
        initRepository.delete(temp);;;
        return InitUtils.toDto(temp);
    }

    private Init getEntityById(Long id){
        return initRepository.findById(id).orElseThrow(() -> new EntityNotFoundException());
    }


}
