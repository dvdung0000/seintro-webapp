package com.example.intro.se.service.impl;

import com.example.intro.se.domain.User;
import com.example.intro.se.dto.request.UserCreateRequestDTO;
import com.example.intro.se.dto.request.UserUpdateRequestDTO;
import com.example.intro.se.dto.response.UserResponseDTO;
import com.example.intro.se.repository.UserRepository;
import com.example.intro.se.service.UserService;
import com.example.intro.se.utils.UserUtils;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;

@AllArgsConstructor
@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    @Override
    public Page<UserResponseDTO> getAll(Pageable pageable) {
        return userRepository.findAll(pageable).map(UserUtils::toDTO);
    }

    @Override
    public UserResponseDTO getById(Long id) {
        return UserUtils.toDTO(getUserById(id));
    }

    @Override
    public UserResponseDTO update(Long id, UserUpdateRequestDTO updateRequestDTO) {
        User user = getUserById(id);
        return UserUtils.toDTO(userRepository.save(UserUtils.toDAO(user, updateRequestDTO)));
    }

    @Override
    public UserResponseDTO create(Long id, UserCreateRequestDTO createRequestDTO) {
        if(isExist(id)){
            throw new EntityExistsException("Entity with id " + id +" already existed");
        }
        return UserUtils.toDTO(userRepository.save(UserUtils.toDAO(createRequestDTO)));
    }

    @Override
    public void delete(Long id) {
        getUserById(id);
        userRepository.deleteById(id);
    }

    private boolean isExist(Long id){
        return userRepository.findById(id).isPresent();
    }

    private User getUserById(Long id){
        return userRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Entity with id " + id + " not found"));
    }
}
