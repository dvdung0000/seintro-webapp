package com.example.intro.se.service;


import com.example.intro.se.dto.request.InitCreateRequestDTO;
import com.example.intro.se.dto.request.InitUpdateRequestDTO;
import com.example.intro.se.dto.response.InitResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface InitService {
    Page<InitResponseDTO> findAll(Pageable pageable);
    InitResponseDTO create(InitCreateRequestDTO initCreateRequestDTO);
    InitResponseDTO update(Long id, InitUpdateRequestDTO initUpdateRequestDTO);
    InitResponseDTO delete(Long id);
}
