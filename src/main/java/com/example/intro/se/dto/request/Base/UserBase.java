package com.example.intro.se.dto.request.Base;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserBase{

    @NonNull
    private String name;

    @NonNull
    private String gender;

    @NonNull
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date birthday;

    @NonNull
    private String address;

    @NonNull
    private String telNo;
}
