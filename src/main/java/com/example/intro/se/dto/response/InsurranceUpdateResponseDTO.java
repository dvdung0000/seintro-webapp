package com.example.intro.se.dto.response;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class InsurranceUpdateResponseDTO {
    private Long id;

    private Long userId;


    private String newInsurranceId;
}
