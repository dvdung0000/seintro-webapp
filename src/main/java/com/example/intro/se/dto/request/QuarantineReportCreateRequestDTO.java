package com.example.intro.se.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class QuarantineReportCreateRequestDTO{

    private String type;

    private String addr;

    @ApiModelProperty(example = "dd/MM/yyyy")
    @JsonFormat(pattern="dd/MM/yyyy")
    private Date time;

    private String room;

    private String bed;

    private List<CovidTestCreateRequestDTO> tests;

}
