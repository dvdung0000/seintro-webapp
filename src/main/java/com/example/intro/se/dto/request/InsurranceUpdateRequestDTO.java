package com.example.intro.se.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class InsurranceUpdateRequestDTO {

    private Long id;

    private Long userId;

    private String insurranceId;
}
