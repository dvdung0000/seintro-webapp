package com.example.intro.se.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class HealthReportCreateRequestDTO {

    private String insurranceId;

    private boolean fever;

    private boolean cough;

    private boolean shortWinded;

    private boolean pneumonia;

    private boolean soreThroat;

    private boolean tired;

    private boolean chronicLiver;

    private boolean chronicBlood;

    private boolean chronicLung;

    private boolean chronicKidney;

    private boolean heartRelatedDiseases;
}
