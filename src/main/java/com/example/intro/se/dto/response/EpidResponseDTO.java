package com.example.intro.se.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Builder

public class EpidResponseDTO {
    private Long id;

    private Long userId;

    private String des;

    @JsonFormat(pattern="dd/MM/yyyy")
    private Date arrTime;

    private String vehicle;

    private String vehCode;

    private String recentArr;

    private boolean hasFever;

    private boolean hasCough;

    private boolean isShortWinded;

    private boolean isSkinBlooded;

    private boolean pharmacy;
}
