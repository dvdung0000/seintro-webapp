package com.example.intro.se.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class QuarantineResponseDTO {

    private Long id;

    private Long userId;

    private String type;

    private  String addr;

    @JsonFormat(pattern="dd/MM/yyyy")
    private Date time;

    private String room;

    private String bed;
}
