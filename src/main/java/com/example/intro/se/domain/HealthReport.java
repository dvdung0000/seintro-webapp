package com.example.intro.se.domain;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "health_report")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HealthReport {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "insurrance_id", nullable = true, length = 45)
    private String insurranceId = null;

    @Column(name = "has_fever", nullable = false)
    private boolean fever;

    @Column(name = "has_cough", nullable = false)
    private boolean cough;

    @Column(name = "is_short_winded", nullable = false)
    private boolean shortWinded;

    @Column(name = "has_pneumonia", nullable = false)
    private boolean pneumonia;

    @Column(name = "has_sore_throat", nullable = false)
    private boolean soreThroat;

    @Column(name = "is_tired", nullable = false)
    private boolean tired;

    @Column(name = "has_chronic_liver", nullable = false)
    private boolean chronicLiver;

    @Column(name = "has_chronic_blood", nullable = false)
    private boolean chronicBlood;

    @Column(name = "has_chronic_lung", nullable = false)
    private boolean chronicLung;

    @Column(name = "has_chronic_kidney", nullable = false)
    private boolean chronicKidney;

    @Column(name = "has_heart_related__diseases", nullable = false)
    private boolean heartRelatedDiseases;
}
