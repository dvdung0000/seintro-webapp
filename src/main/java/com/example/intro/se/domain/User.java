package com.example.intro.se.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User{
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "name", length = 45, nullable = false)
    private String name;

    @Column(name = "gender", length = 15, nullable = false)
    private String gender;

    @Column(name = "date_of_birth", nullable = false)
    private Date birthday;

    @Column(name = "address", nullable = false, length = 45)
    private String address;

    @Column(name = "tel_no",nullable = false,length = 15)
    private String telNo;
}
