package com.example.intro.se.utils.sendmail;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class SendGmail {
	// public static void main(String args[]) throws AddressException, MessagingException {
	// 	sendText();
	// 	sendHTML();
	// 	sendFile();
	// 	System.out.println("done");
	// }

	public void sendHTML(String receiver) throws AddressException, MessagingException {
		Properties mailServerProperties;
		Session getMailSession;
		MimeMessage mailMessage;

		// setup Mail Server
		mailServerProperties = System.getProperties();
		mailServerProperties.put("mail.smtp.port", "587");
		mailServerProperties.put("mail.smtp.auth", "true");
		mailServerProperties.put("mail.smtp.starttls.enable", "true");

		// get Mail Session
		getMailSession = Session.getDefaultInstance(mailServerProperties, null);
		mailMessage = new MimeMessage(getMailSession);

		mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(receiver)); 

		mailMessage.setSubject("Succesful Registration");
		
		StringBuilder contentBuilder = new StringBuilder();
		try {
			BufferedReader in = new BufferedReader(new FileReader("lib/thankyou.html"));
			System.out.println(in.readLine());
    		String str;
    		while ((str = in.readLine()) != null) {
        		contentBuilder.append(str);
    		}
    		in.close();
		} catch (IOException e) {
		}
        String content = contentBuilder.toString();
		
		// text Part
		BodyPart messagePart = new MimeBodyPart();
		messagePart.setText("Thank you for your contribution");
		
		//html Part
		BodyPart htmlPart = new MimeBodyPart();
		htmlPart.setContent(content, "text/html");

		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messagePart);
		multipart.addBodyPart(htmlPart);
		mailMessage.setContent(multipart);			

		// send Mail
		Transport transport = getMailSession.getTransport("smtp");

		transport.connect("smtp.gmail.com", "se.intro.20201@gmail.com", "nooneisgreat");
		transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
		transport.close();
	}

	// public static void sendFile() throws AddressException, MessagingException {
	// 	Properties mailServerProperties;
	// 	Session getMailSession;
	// 	MimeMessage mailMessage;

	// 	// Step1: setup Mail Server
	// 	mailServerProperties = System.getProperties();
	// 	mailServerProperties.put("mail.smtp.port", "587");
	// 	mailServerProperties.put("mail.smtp.auth", "true");
	// 	mailServerProperties.put("mail.smtp.starttls.enable", "true");

	// 	// Step2: get Mail Session
	// 	getMailSession = Session.getDefaultInstance(mailServerProperties, null);
	// 	mailMessage = new MimeMessage(getMailSession);

		
	// 	// thay abc@gmail.com bằng địa chỉ người nhận
	// 	mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("abc@gmail.com"));
	// 	mailMessage.setSubject("Demo send gmail from Java");
		
	// 	// Bạn có thể chọn CC, BCC
	// 	// generateMailMessage.addRecipient(Message.RecipientType.CC, new InternetAddress("cc@gmail.com")); //Địa chỉ cc gmail

	// 	// Tạo phần gửi message
	// 	BodyPart messagePart = new MimeBodyPart();
	// 	messagePart.setText("Demo send file by Gmail from Java");

	// 	// Tạo phần gửi file
	// 	BodyPart filePart = new MimeBodyPart();
		
	// 	// thay C:\\a.txt thành file mà bạn muốn gửi
	// 	File file = new File("C:\\a.txt");
	// 	DataSource source = new FileDataSource(file);
	// 	filePart.setDataHandler(new DataHandler(source));
	// 	filePart.setFileName(file.getName());

	// 	// Gộp message và file vào để gửi đi
	// 	Multipart multipart = new MimeMultipart();
	// 	multipart.addBodyPart(messagePart);
	// 	multipart.addBodyPart(filePart);
	// 	mailMessage.setContent(multipart);

	// 	// Step3: Send mail
	// 	Transport transport = getMailSession.getTransport("smtp");

	// 	// Thay your_gmail thành gmail của bạn, thay your_password thành mật khẩu gmail của bạn
	// 	transport.connect("smtp.gmail.com", "your_gmail", "your_password");
	// 	transport.sendMessage(mailMessage, mailMessage.getAllRecipients());
	// 	transport.close();
	// }
}